import pandas as pd
from statsmodels.tsa.statespace.sarimax import SARIMAX
from statsmodels.tsa.arima_model import ARIMA
import matplotlib.pyplot as plt
import numpy as np
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.graphics.tsaplots import plot_pacf
from sklearn.metrics import mean_squared_error


class Arima:
    def __init__(self, create_dataset=False):

        # DataFrames with the data

        self.df = pd.DataFrame()

        if create_dataset:
            self.get_info()
            self.predict_missing_values()
        self.build_sets()

        self.values = list(self.df['Average_o3'])

    def get_info(self):
        df = pd.read_csv("../Data/o3/o3.csv", sep='\t')

        new_df = df[['o3', 'Year', 'Month', 'Day']].groupby(['Year', 'Month', 'Day']).mean()
        new_df = new_df.reset_index()
        new_df = new_df.rename(columns={'o3': 'Average'})

        df_min = df[['o3', 'Year', 'Month', 'Day']].groupby(['Year', 'Month', 'Day']).min()
        df_min = df_min.reset_index()
        df_min = df_min.rename(columns={'o3': 'Min'})

        new_df['Min'] = df_min['Min']

        df_max = df[['o3', 'Year', 'Month', 'Day']].groupby(['Year', 'Month', 'Day']).max()
        df_max = df_max.reset_index()
        df_max = df_max.rename(columns={'o3': 'Max'})

        new_df['Max'] = df_max['Max']

        df_variance = df[['o3', 'Year', 'Month', 'Day']].groupby(['Year', 'Month', 'Day']).var()
        df_variance = df_variance.reset_index()
        df_variance = df_variance.rename(columns={'o3': 'Variance'})

        new_df['Variance'] = df_variance['Variance']

        new_df.to_csv("../Data/o3/o3_not_scaled.csv", sep='\t', encoding='utf-8')

    def seasonal_plot(self):

        df = self.df

        values = list(df['Average_o3'])

        my_tics = ['2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']

        array = []
        for i in range(len(my_tics)):
            array.append(365*i)

        plt.clf()
        title = "Average level of ozone per day"
        plt.title(title)
        plt.xlabel("Years")
        plt.ylabel("Average Level of Ozone")
        plt.xticks(array, my_tics, rotation=45)
        plt.plot(np.arange(len(values)), values)
        plt.savefig("../results/images/total_values.png")
        # plt.show()

        year_values = df.loc[df['Year'] == 2007]

        # print(year_values.loc[year_values['Month'] == 2])

        year_values_list = list(year_values['Average_o3'])

        my_tics = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

        plt.clf()
        title = "Average Level of Ozone - 2007"
        plt.title(title)
        plt.xlabel("Months")
        plt.ylabel("Average Level of Ozone")
        plt.xticks([0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334], my_tics)
        plt.plot(np.arange(len(year_values_list)), year_values_list)
        plt.savefig("../results/images/year_values.png")
        # plt.show()

        month_values = list(year_values.loc[year_values['Month'] == 2]['Average_o3'])
        plt.clf()
        title = "Average level of Ozone of o3 - 02/2007"
        plt.title(title)
        plt.xlabel("Days")
        plt.ylabel("Average Level of Ozone")
        plt.plot(np.arange(len(month_values)), month_values)
        plt.savefig("../results/images/month_values.png")
        # plt.show()

    def predict_missing_values(self):
        df = pd.read_csv("../Data/o3/o3_not_scaled.csv", sep='\t')

        df['Average_interpolate'] = df['Average']
        df['Average_rolling'] = df['Average']
        df['Min_interpolate'] = df['Min']
        df['Min_rolling'] = df['Min']
        df['Max_interpolate'] = df['Max']
        df['Max_rolling'] = df['Max']
        df['Variance_interpolate'] = df['Variance']
        df['Variance_rolling'] = df['Variance']

        df['Average_rolling'] = df.Average_rolling.apply(lambda x: np.nan if pd.isnull(x) else x)
        # print(df['Average_rolling'][44])
        df['Average_rolling'] = df.Average_rolling.fillna(df.Average_rolling.rolling(4, min_periods=1).mean())
        # print(df['Average_rolling'][44])

        df['Min_rolling'] = df.Min_rolling.apply(lambda x: np.nan if pd.isnull(x) else x)
        df['Min_rolling'] = df.Min_rolling.fillna(df.Min_rolling.rolling(4, min_periods=1).mean())

        df['Max_rolling'] = df.Max_rolling.apply(lambda x: np.nan if pd.isnull(x) else x)
        df['Max_rolling'] = df.Max_rolling.fillna(df.Min_rolling.rolling(4, min_periods=1).mean())

        df['Variance_rolling'] = df.Variance_rolling.apply(lambda x: np.nan if pd.isnull(x) else x)
        df['Variance_rolling'] = df.Variance_rolling.fillna(df.Min_rolling.rolling(4, min_periods=1).mean())

        df['Average_interpolate'].interpolate(method='linear', inplace=True)
        df['Min_interpolate'].interpolate(method='linear', inplace=True)
        df['Max_interpolate'].interpolate(method='linear', inplace=True)
        df['Variance_interpolate'].interpolate(method='linear', inplace=True)

        # df['Average_rolling'] = old_df['col']

        # df.shift().rolling(window=4, min_periods=1).mean()

        df['Predicted'] = df['Min'] != df['Min_interpolate']

        df.to_csv("../Data/o3/o3_not_scaled_info.csv", sep='\t', encoding='utf-8', float_format='%.6f')

    def build_sets(self):

        tdf = pd.read_csv("../Data/o3/o3_info.csv", sep='\t')
        # print(tdf)
        tdf = tdf.rename(index=str, columns={"Average_interpolate": "Average_o3",
                                             "Min_interpolate": "Min_o3",
                                             "Max_interpolate": "Max_o3",
                                             "Variance_interpolate": "Variance_o3"})
        del tdf['Average']
        del tdf['Min']
        del tdf['Max']
        del tdf['Variance']
        del tdf['Average_rolling']
        del tdf['Min_rolling']
        del tdf['Max_rolling']
        del tdf['Predicted']
        # df['Id'] = range(0, len(df))
        tdf.reset_index()
        # df.set_index('Unnamed: 0_x')
        for col in tdf.columns:
            if 'Unnamed' in col:
                try:
                    del tdf[col]
                except:
                    j = 0
        df = tdf.loc[tdf['Year'] > 2006]
        df = df.loc[df['Year'] < 2018]

        self.df = df

    # create a differenced series
    def difference(self, interval=0):
        df = self.df
        diff = dict()
        # df.iloc[[2]]
        start = 2007

        # real_values = list(df['Average_o3'])

        real_values = self.values

        for i in range(interval, len(df)):
            elem = df.iloc[[i]]
            value = elem['Average_o3'].iloc[0]
            if int(elem['Year'].iloc[0]) > start:

                prev_year = df.loc[df['Year'] == int((elem['Year'].iloc[0]))-1]
                prev_month = prev_year.loc[prev_year['Month'] == int((elem['Month'].iloc[0]))]
                prev_value = prev_month.loc[prev_year['Day'] == int((elem['Day'].iloc[0]))]

                if (elem['Month'].iloc[0]) == 2 and elem['Day'].iloc[0] == 29:
                    previous_id = str((elem['Year'].iloc[0])-1) + "_" + str((elem['Month'].iloc[0])) + "_" \
                                  + str(28)
                    prev_value = prev_month.loc[prev_year['Day'] == 28]
                else:
                    previous_id = str((elem['Year'].iloc[0])-1) + "_" + str((elem['Month'].iloc[0])) + "_" \
                              + str((elem['Day'].iloc[0]))

                # print(prev_value['Average_o3'])

                value = elem['Average_o3'].iloc[0] - prev_value['Average_o3'].iloc[0]

            _id = str(elem['Year'].iloc[0]) + "_" + str(elem['Month'].iloc[0]) + "_" + str(elem['Day'].iloc[0])
            print(_id)
            diff[_id] = value

        values = []
        file = open("test.txt", "w+")
        for key, value in diff.items():
            values.append(value)
        file.write(str(values))
        file.close()
        plt.clf()
        plt.plot(np.arange(len(values)), values)
        plt.savefig("../results/images/modified_stationary_data.png")

        plt.clf()
        plt.plot(np.arange(len(real_values)), real_values)
        plt.savefig("../results/images/stationary_data.png")
        self.values = np.array(values)

    def test_x(self):

        # self.difference()
        # self.difference()

        differenced = self.values
        # differenced = list(self.df['Average_o3'])

        size = int(len(differenced) * 0.99)

        # print("Differenced:", differenced)

        y_train, y_test = differenced[0:size], differenced[size:len(differenced)]

        history = [x for x in y_train]
        predictions = list()

        print("Length of the test:", len(y_test))

        error = []

        #model = ARIMA(history, order=(0, 0, 0))
        model = SARIMAX(history, order=(4, 1, 0), seasonal_order=(0, 1, 0, 365))
        model_fit = model.fit(disp=0)

        for t in range(len(y_test)):
            output = model_fit.forecast()
            yhat = output[0]
            predictions.append(yhat)
            obs = y_test[t]
            history.append(obs)
            print(str(t)+') predicted=%f, expected=%f' % (yhat, obs))
            error.append(yhat[0]-obs)

        mse = ((np.array(y_test) - np.array(predictions)) ** 2).mean(axis=None)
        print('Test MSE: %.10f' % mse)
        # plot
        plt.clf()
        plt.plot(np.arange(len(y_test)), y_test, label='true values')
        plt.plot(np.arange(len(predictions)), predictions, label='predictions')
        plt.title('MSE: %.6f' % mse)
        plt.legend()
        plt.savefig("../results/images/ARIMA_stationary_result.png")

        plt.clf()
        plt.plot(np.arange(len(error)), error, label='error')
        plt.title("ERRORE")
        plt.legend()
        plt.show()
        # plt.savefig("../results/images/ARIMA_stationary_error.png")

        print(error)

        series = pd.Series(np.array(error))

        plot_acf(series, lags=100)
        plt.show()
        plt.clf()
        plot_pacf(pd.Series(np.array(error)), lags=100)
        plt.show()

    def test_y(self):
        df = self.df

        values = df['Average_o3']

        size = int(len(values) * 0.99)

        y_train, y_test = values[0:size], values[size:len(values)]

        mod = SARIMAX(list(df['Average_o3']), order=(4, 1, 0), seasonal_order=(0, 1, 0, 365))
        results = mod.fit(maxiter=5)
        print("ciao")
        print(results.summary())

        error = []
        predictions = []

        for t in range(len(y_test)):
            output = results.predict(start=size, end=values)
            yhat = output[0]
            predictions.append(yhat)
            obs = y_test[t]
            # history.append(obs)
            print(str(t)+') predicted=%f, expected=%f' % (yhat, obs))
            error.append(yhat[0]-obs)

        mse = ((np.array(y_test) - np.array(predictions)) ** 2).mean(axis=None)
        print('Test MSE: %.10f' % mse)
        # plot
        plt.clf()
        plt.plot(np.arange(len(y_test)), y_test, label='true values')
        plt.plot(np.arange(len(predictions)), predictions, label='predictions')
        plt.title('MSE: %.6f' % mse)
        plt.legend()
        plt.savefig("../results/images/ARIMA_stationary_result.png")

    def test_not_stationary(self):

        Y = list(self.df['Average_o3'])

        size = int(len(Y) * 0.80)

        y_train, y_test = Y[0:size], Y[size:len(Y)]
        history = [x for x in y_train]
        predictions = list()

        print("Length of the test:", len(y_test))

        for t in range(len(y_test)):
            model = ARIMA(history, order=(4,0,1))
            # model_fit = model.fit(disp=0, transparams=False) 0.0055448595
            # model_fit = model.fit(disp=0, trend='c') We can see the effect clearly if we rerun the original example
            # and print the model coefficients for each step of the walk-forward validation and compare the same with
            # the trend term turned off.
            model_fit = model.fit(disp=0, trend='nc')  # 0.0026311194
            output = model_fit.forecast()
            yhat = output[0]
            predictions.append(yhat)
            obs = y_test[t]
            history.append(obs)
            print(str(t)+') predicted=%f, expected=%f' % (yhat, obs))

        mse = ((np.array(y_test) - np.array(predictions)) ** 2).mean(axis=None)
        print('Test MSE: %.10f' % mse)
        # plot
        plt.clf()
        plt.scatter(np.arange(len(y_test)), y_test, label='true values')
        plt.scatter(np.arange(len(predictions)), predictions, label='predictions')
        plt.title('MSE: %.6f' % mse)
        plt.legend()
        plt.savefig("../results/images/ARIMA_not_stationary_result.png")

    # evaluate an ARIMA model for a given order (p,d,q)
    def evaluate_arima_model(self, X, arima_order):
        # prepare training dataset
        train_size = int(len(X) * 0.90)
        train, test = X[0:train_size], X[train_size:]
        history = [x for x in train]
        # make predictions
        predictions = list()
        for t in range(len(test)):
            model = ARIMA(history, order=arima_order)
            model_fit = model.fit(disp=0)
            yhat = model_fit.forecast()[0]
            predictions.append(yhat)
            history.append(test[t])
        # calculate out of sample error
        error = mean_squared_error(test, predictions)
        return error

    def evaluate_models(self, p_values, d_values, q_values):
        df = self.df

        values = list(df['Average_o3'])[df.shape[0]-2040:df.shape[0]]
        best_score, best_cfg = float("inf"), None
        for p in p_values:
            for d in d_values:
                for q in q_values:
                    order = (p,d,q)
                    try:
                        mse = self.evaluate_arima_model(values, order)
                        if mse < best_score:
                            best_score, best_cfg = mse, order
                        print('ARIMA%s MSE=%.8f' % (order,mse))
                    except:
                        continue
        print('Best ARIMA%s MSE=%.6f' % (best_cfg, best_score))  # Best ARIMA(2, 0, 1) MSE=0.00226608

        self.test(best_cfg)
        # plot

    def test(self, best_cfg=(2,0,1)):

        df = self.df

        values = list(df['Average_o3'])[df.shape[0]-2040:df.shape[0]]
        train_size = int(len(values) * 0.96)
        train, test = values[0:train_size], values[train_size:]
        history = [x for x in train]
        # make predictions
        predictions = list()
        error = []

        print("PREDICTIONS:", len(test))

        for t in range(len(test)):
            model = ARIMA(history, order=best_cfg)
            model_fit = model.fit(disp=0)
            yhat = model_fit.forecast()[0]
            predictions.append(yhat)
            history.append(test[t])
            error.append(yhat[0]-test[t])
            print(str(t)+') predicted=%f, expected=%f' % (yhat, test[t]))

        mse = ((np.array(test) - np.array(predictions)) ** 2).mean(axis=None)

        plt.clf()
        # plt.scatter(np.arange(len(test)), test, label='true values')
        plt.plot([0, max(predictions)], [0, max(predictions)], label='bisector')
        plt.scatter(test, predictions, label='test-predictions', color='r')
        plt.xlabel("true values")
        plt.ylabel("predicted values")
        plt.title("True vs Predicted (MSE: %.6f)"% mse)
        plt.legend()
        plt.savefig("../results/images/ARIMA_result.png")

