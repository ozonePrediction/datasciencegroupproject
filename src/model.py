import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.model_selection import KFold, cross_val_score
import seaborn as sns
import matplotlib.patches as mpatches
import ast
import collections
from scipy.signal import savgol_filter


class Model:

    def __init__(self, files, c_range):

        # Take all the necessary files

        self.files = files

        self.data_file = []
        for file in files:
            self.data_file.append((file, file + "_info"))

        # DataFrames with the data

        self.X = pd.DataFrame()

        self.y = pd.DataFrame()

        # models: it is an array because could be useful to compare the results then

        self.models = []
        functions = ['rbf', 'sigmoid']
        for c in c_range:
        # for f in functions:
            self.models.append(('SVR_'+str(c), SVR(C=c, kernel='rbf', epsilon=0.001)))

        self.results = {}

        self.X_features = []

    def build_sets(self):
        df = pd.DataFrame()
        for i, file in enumerate(self.data_file):
            tdf = pd.read_csv("../Data/" + file[0] + "/" + file[1] + ".csv", sep='\t')
            tdf = tdf.rename(index=str, columns={"Average_interpolate": "Average_" + file[0],
                                                 "Min_interpolate": "Min_" + file[0],
                                                 "Max_interpolate": "Max_" + file[0],
                                                 "Variance_interpolate": "Variance_" + file[0]})
            del tdf['Average']
            del tdf['Min']
            del tdf['Max']
            del tdf['Variance']
            del tdf['Variance_rolling']
            del tdf['Average_rolling']
            del tdf['Min_rolling']
            del tdf['Max_rolling']
            del tdf['Predicted']
            if i == 0:
                df = tdf
            else:
                df = df.merge(tdf, 'outer', on=["Year", "Month", "Day"])
        # df['Id'] = range(0, len(df))
        df.reset_index()
        # df.set_index('Unnamed: 0_x')
        for col in df.columns:
            if 'Unnamed' in col:
                try:
                    del df[col]
                except:
                    j = 0
        df = df.loc[df['Year'] > 2006]
        df = df.loc[df['Year'] < 2018]

        self.X = df
        self.X = self.X.drop(columns='Average_o3')
        self.X = self.X.drop(columns='Min_o3')
        self.X = self.X.drop(columns='Max_o3')
        self.X = self.X.drop(columns='Variance_o3')
        self.X.to_csv("../Data/train_x.csv", sep='\t', encoding='utf-8', index=False)

        self.y = df['Average_o3']

    def train_and_select(self, features, scoring):
        """
        This function will train and test the data according to the models chosen.
        :return:
        """
        validation_size = 0.20
        seed = 4
        X_train, X_validation, y_train, y_validation = train_test_split(self.X, self.y,
                                                                        test_size=validation_size, random_state=seed)

        x_train = pd.DataFrame()
        for c in features:
            x_train[c] = X_train[c]

        # x_train['Min_temp_Average_temp'] = self.X['Min_temp']*self.X['Average_temp']

        # x_train['Average_no_Average_nox'] = X_train['Average_no'] * X_train['Month']
        # x_train['Month_max_no'] = X_train['Month'] * X_train['Max_nox']

        # Test options and evaluation metric
        num_folds = 10

        max_explained_var = -1
        max_name = None
        max_model = None
        plt.clf()
        # plt.scatter(np.arange(len(y_validation)), y_validation, label="real values", color='black')

        values_dict = {}

        y_validation = list(y_validation)

        for name, model in self.models:
            print("MODEL:", name)
            k_fold = KFold(n_splits=num_folds, random_state=seed)
            cv_results = cross_val_score(model, x_train, y_train, cv=k_fold, scoring=scoring)
            # print("cv_results:", cv_results)
            self.results[name] = cv_results
            # names.append(name)
            # msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
            # print(msg)

            if cv_results.mean() > max_explained_var:
                max_explained_var = cv_results.mean()
                max_name = name
                max_model = model
            model.fit(X_train, y_train)
            y_pred = model.predict(X_validation)

            for i in range(len(y_pred)):
                values_dict[y_validation[i]] = y_pred[i]

            od = collections.OrderedDict(sorted(values_dict.items()))

            # print(list(od.values()))

            yhat = savgol_filter(list(od.values()), 51, 3)

            # print(x_new, y_smooth)
            plt.plot(od.keys(), yhat, label=name)

        plt.plot([0, max(y_validation)], [0, max(y_validation)])

        plt.xlabel("True values")
        plt.ylabel("Predicted values")
        plt.legend(['SVR_5', 'SVR_6', 'SVR_7', 'SVR_8', 'SVR_9'])
        plt.title("True values vs Predicted values")
        plt.savefig("../results/images/score.png")

        print("Max variance:", max_explained_var)
        print("Max name:", max_name)

        return max_model

    def test(self, scoring):
        validation_size = 0.20
        seed = 20
        X_train, X_validation, y_train, y_validation = train_test_split(self.X, self.y,
                                                                        test_size=validation_size, random_state=seed)

        file = open("../results/selected_features.txt", "r")
        values_dict = ast.literal_eval(file.readline())

        x_train = pd.DataFrame()
        x_validation = pd.DataFrame()
        features = []
        for key, value in values_dict.items():
            x_train[key] = X_train[key]
            x_validation[key] = X_validation[key]
            features.append(key)

        # get the max model
        max_model = self.train_and_select(features=features, scoring=scoring)

        # training
        print("TRAINING THE MAX MODEL:", max_model)

        # add a new feature based on previous ozone level day
        values_train = []
        values_validation = []
        for i in range(4):
            values_train.append(i)
            values_validation.append(i)
        for i in range(4, len(y_train)):
            x = 0
            for j in range(4):
                x = x + y_train.iloc[i-j]
            values_train.append(np.mean(x))

        for i in range(4, len(y_validation)):
            x_val = 0
            for j in range(4):
                x_val = x_val + y_validation.iloc[i-j]
            values_validation.append(np.mean(x_val))

        # adding the values for in the training set
        x_train['Ozone'] = values_train
        x_validation['Ozone'] = values_validation

        # fit the max model
        max_model.fit(x_train, y_train)
        predictions = np.array(list(max_model.predict(x_validation)))

        y_validation_list = np.array(list(y_validation))

        # calculate the MSE for this model
        mse = ((predictions - y_validation_list) ** 2).mean(axis=None)

        print("PREDICTION MSE:", mse)

        plt.clf()
        # plt.scatter(np.arange(len(test)), test, label='true values')
        plt.plot([0, max(predictions)], [0, max(predictions)], label='bisector')
        plt.scatter(y_validation, predictions, label='test-predictions', color='r')
        plt.xlabel("true values")
        plt.ylabel("predicted values")
        plt.title("True vs Predicted (MSE: %.6f)" % mse)
        plt.legend()
        plt.savefig("../results/images/SVR_result.png")

        self.show_importance()

    def compare_algorithms(self):
        """
        This function will show the result and comparison between the models chosen to predict the y value.
        :return:
        """
        # get the names
        names = []
        results = []
        for key, value in self.results.items():
            names.append(key)
            results.append(value)

        # Compare Algorithms
        plt.clf()
        fig = plt.figure()
        fig.suptitle('Scaled Algorithm Comparison')
        ax = fig.add_subplot(111)
        plt.boxplot(results)

        ax.set_xticklabels(names)
        plt.savefig("../results/images/comparison.png")

    def feature_selection(self):

        validation_size = 0.20
        seed = 4

        X_train, X_validation, y_train, y_validation = train_test_split(self.X, self.y,
                                                                        test_size=validation_size, random_state=seed)
        x_df = pd.DataFrame()

        num_folds = 4
        scoring = 'r2'

        columns = []
        values = []

        selected_features = {}

        x_train_columns = list(X_train.columns)
        previous = -1

        for column in X_train.columns:

            max_value = -1
            max_col = None
            for col in x_train_columns:
                x_df_temp = pd.DataFrame()

                for c in x_df.columns:
                    x_df_temp[c] = x_df[c]

                x_df_temp[col] = X_train[col]
                print("Training model:", list(x_df_temp.columns))
                for name, model in self.models:
                    k_fold = KFold(n_splits=num_folds, random_state=seed)
                    cv_results = cross_val_score(model, x_df_temp, y_train, cv=k_fold, scoring=scoring)
                    # print("cv_results:", cv_results)
                    self.results[name] = cv_results
                    if cv_results.mean() > max_value:
                        max_value = cv_results.mean()
                        max_col = col
                    # names.append(name)
                    # msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
                    # print(msg)
            print("MAX_COL:", max_col)
            print("MAX_VALUE:", max_value)
            if max_value > previous > 0 or previous == -1:
                selected_features[max_col] = max_value
                print("Selected features:", selected_features)
            previous = max_value

            columns.append(max_col)
            values.append(max_value)
            print("MAX_VALUES:", values)
            x_train_columns.remove(max_col)
            x_df[max_col] = X_train[max_col]

        plt.clf()
        fig = plt.figure()
        fig.suptitle('Improvements')
        ax = fig.add_subplot(111)
        print(values)
        plt.plot(np.arange(len(values)), values)
        plt.xlabel("number of features")
        plt.ylabel("crossed R2")
        plt.savefig("../results/images/selection.png")

        print("COLUMNS:", columns)

        file = open("../results/selected_features.txt", "w+")
        file.write(str(selected_features))

        file.close()

    def show_importance(self):
        file = open("../results/selected_features.txt", "r")

        values_dict = ast.literal_eval(file.readline())
        values = []

        file.close()

        for key, value in values_dict.items():
            self.X_features.append(key)
            values.append(value)

        # values = [0.3609276344280596, 0.49830324018586886, 0.5633352065811719, 0.6034624548271708, 0.6254178451360315,
                  # 0.6379459962103634, 0.6401535329064654]

        real_values = [values[0]]
        for i in range(len(values)):
            if i > 0:
                real_values.append(values[i]-values[i-1])
        data = pd.DataFrame({
            'importance': real_values,
            'feature_name': self.X_features
        })

        plt.clf()
        fig, ax = plt.subplots()
        ax = sns.barplot(x='importance', y='feature_name', data=data, ax=ax)
        ax.set_yticklabels('')

        # There is no labels, need to define the labels
        legend_labels = self.X_features

        Boxes = [item for item in ax.get_children()
                 if isinstance(item, mpatches.Rectangle)][:-1]

        # Create the legend patches
        legend_patches = [mpatches.Patch(color=C, label=L) for
                          C, L in zip([item.get_facecolor() for item in Boxes],
                                      legend_labels)]

        # Put the legend out of the figure
        plt.legend(handles=legend_patches)
        plt.title("Importance of features")

        plt.savefig("../results/images/features_importance.png")

    def print_correlation(self):
        cols = ['Month', 'Max_no', 'Average_temp', 'Min_temp', 'Average_nox', 'Max_nox', 'Average_no', 'Min_nox']

        x_df = pd.DataFrame()
        for c in cols:
            x_df[c] = self.X[c]

        for col in cols:
            for c in cols:
                if col != c:
                    plt.clf()
                    plt.scatter(self.X[col], self.X[c])
                    plt.xlabel(col)
                    plt.ylabel(c)
                    plt.savefig("../Data/images/correlations/"+c+"_"+col)

        corr = x_df.corr(method='pearson')
        sns.heatmap(corr)

        plt.savefig("../Data/images/correlations/heatmap.png")

